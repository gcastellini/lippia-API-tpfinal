package ar.validator;

import api.model.TimeEntry.TimeEntryResponse;
import com.crowdar.api.rest.APIManager;
import org.testng.Assert;

public class TimeEntryValidator {

    public static void validate() {
        TimeEntryResponse[] response = (TimeEntryResponse[]) APIManager.getLastResponse().getResponse();
        Assert.assertNotNull(response[0].getId());
    }

    public static void validateNewEntry(String userId, String project) {
        TimeEntryResponse response = (TimeEntryResponse) APIManager.getLastResponse().getResponse();
        Assert.assertTrue(response.getUserId().equals(userId) && response.getProjectId().equals(project));

    }

    public static void validateDescription(String desc) {
        TimeEntryResponse response = (TimeEntryResponse) APIManager.getLastResponse().getResponse();
        Assert.assertEquals(desc, response.getDescription());
    }

    public static void validateNull() {
        TimeEntryResponse[] response = (TimeEntryResponse[]) APIManager.getLastResponse().getResponse();
        Assert.assertNull(response);

    }
}


