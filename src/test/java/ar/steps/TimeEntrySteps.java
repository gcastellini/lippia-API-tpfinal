package ar.steps;

import com.crowdar.core.PageSteps;
import ar.validator.TimeEntryValidator;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.jsoup.Connection;
import services.BaseService;

import java.sql.Time;

public class TimeEntrySteps extends PageSteps {

    @When("tengo un user (.*) y un proyecto (.*)")
    public void setUserandProject(String user,String project){
        BaseService.USER_ID.set(user);
        BaseService.ID_PROJ.set(project);

    }

    @When("tengo el ID de un registro de horas (.*)")
    public void setTimeEntryID(String id){
        BaseService.TIME_ENTRY_ID.set(id);
    }

    @And("quiero editar la descripcion (.*)")
    public void setDesc(String desc){
        BaseService.DESCRIPTION.set(desc);
    }
    @Then("obtengo los time entry del user")
    public void validate(){
        TimeEntryValidator.validate();

    }

    @Then("valido que se haya agregado el time entry para user (.*) y proyecto (.*)")
    public void validateNewEntry(String userId, String start){
        TimeEntryValidator.validateNewEntry(userId,start);
    }

    @Then("valido que se haya cambiado la description (.*)")
    public void validateDesc(String desc){
        TimeEntryValidator.validateDescription(desc);
    }

    @Then("valido que la respuesta es nula")
    public void validateNull(){
        TimeEntryValidator.validateNull();
    }
}
