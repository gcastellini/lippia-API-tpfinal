Feature: Time Entry

  Background:
    Given Mi cuenta creada en clockify y mi X-Api-Key generada

  @Get
  Scenario Outline: Consulta horas registradas
    When I perform a '<operation>' to '<entity>' endpoint with the '<jsonName>' and ''
    Then se obtuvo el status code <status>
    And obtengo los time entry del user

    Examples:
      | operation | entity       | jsonName       | status |
      | GET       | TIME_ENTRIES | timeentries/rq | 200    |

  @Add
  Scenario Outline: Agregar horas a un proyecto
    When tengo un user <userId> y un proyecto <projectId>
    And I perform a '<operation>' to '<entity>' endpoint with the '<jsonName>' and ''
    Then se obtuvo el status code <status>
    And valido que se haya agregado el time entry para user <userId> y proyecto <projectId>


    Examples:
      | operation | entity     | jsonName            | status | userId                   | projectId                |
      | POST      | TIME_ENTRY | timeentries/POST/rq | 201    | 633f5b7689bf9c24493ee39b | 63795874f570a5720b6414f1 |

  @Edit
  Scenario Outline: Editar regitro de horas
    When tengo el ID de un registro de horas <entryId>
    And quiero editar la descripcion <description>
    And I perform a '<operation>' to '<entity>' endpoint with the '<jsonName>' and ''
    Then se obtuvo el status code <status>
    And valido que se haya cambiado la description <description>

    Examples:
      | operation | entity     | jsonName           | status | entryId                  | description |
      | PUT       | TIME_ENTRY | timeentries/PUT/rq | 200    | 637bf55b4ede403a0ea191a5 | primera     |

  @Delete
  Scenario Outline: Eliminar regitro de horas
    When tengo el ID de un registro de horas <entryId>
    And I perform a '<operation>' to '<entity>' endpoint with the '<jsonName>' and ''
    Then se obtuvo el status code <status>
    And valido que la respuesta es nula

    Examples:
      | operation | entity     | jsonName           | status | entryId                  |
      | DELETE    | TIME_ENTRY | timeentries/PUT/rq | 204    | 637bf55b4ede403a0ea191a5 |