package api.config;

import api.model.ErrorResponse;
import services.*;

public enum EntityConfiguration {

    USER {
        @Override
        public Class<?> getEntityService() {
            return UserService.class;
        }

    },
    RESPONSE_HEADERS {
        @Override
        public Class<?> getEntityService() {
            return ResponseHeadersService.class;
        }
    },
    WORKSPACE {
        @Override
        public Class<?> getEntityService() {
            return WorkspaceService.class;
        }
    },
    ERROR {
        @Override
        public Class<?> getEntityService() {
            return ErrorService.class;
        }
    },
 TIME_ENTRIES{
        @Override
     public Class<?> getEntityService(){return TimeEntriesService.class;}
 },
    TIME_ENTRY{
        @Override
        public Class<?> getEntityService(){return TimeEntryService.class;}
    },
    PROJECTS{
        @Override
        public Class<?> getEntityService(){return ProjectsService.class;}
    },
    PROJECT{
        @Override
                public Class<?> getEntityService(){return ProjectService.class;}
    };
    public abstract Class<?> getEntityService();
}
