package api.model.TimeEntry;

import io.cucumber.java.it.Data;

import java.util.List;


public class TimeEntryResponse {
    private String id;
    private String description;
    private String tagsIds;
    private String userId;
    private boolean billable;
    private String taskId;
    private String projectId;
    private String start;
    private String end;
    private String workspaceId;
    private boolean isLocked;
    private List<String> customFieldValues;
    private String type;
    private String kioskId;


    public String getId(){
        return id;
    }

    public String getUserId(){
        return userId;
    }

    public String getProjectId(){
        return projectId;
    }

    public String getDescription(){
        return description;
    }


}
