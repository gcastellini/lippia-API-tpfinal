package services;

import api.model.TimeEntry.TimeEntryResponse;
import com.crowdar.api.rest.Response;

import java.util.HashMap;
import java.util.Map;

public class TimeEntriesService extends BaseService {
    public static Response get(String jsonName) {
        return get(jsonName, TimeEntryResponse[].class, setParams());
    }


    private static Map<String, String> setParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("api-key", API_KEY.get());
        params.put("workspaceId", "633f5b7689bf9c24493ee39d");
        return params;
    }
}
