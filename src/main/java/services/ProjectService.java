package services;

import api.model.Project.ProjectResponse;
import api.model.workspaces.WorkspacesResponse;
import com.crowdar.api.rest.Response;
import com.crowdar.core.PropertyManager;

import java.util.HashMap;
import java.util.Map;

public class ProjectService extends BaseService{
    public static Response get(String jsonName) {
        return get(jsonName, ProjectResponse.class,setParams());
    }

    private static Map<String, String> setParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("api-key",API_KEY.get());
        params.put("workspaceId","633f5b7689bf9c24493ee39d");
        params.put("projectId",ID_PROJ.get());
        return params;
    }
}
