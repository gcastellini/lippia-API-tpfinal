package services;

import api.model.TimeEntry.TimeEntryResponse;
import com.crowdar.api.rest.Response;

import java.util.HashMap;
import java.util.Map;

public class TimeEntryService extends BaseService{
    public static Response get(String jsonName) {
        return get(jsonName, TimeEntryResponse.class,setParams());
    }

    public static Response post(String jsonName) {
        return post(jsonName, TimeEntryResponse.class, setParamsPost(setParams()));
    }

    public static Response put(String jsonName) {
        return put(jsonName, TimeEntryResponse.class, setParamsPut());
    }

    public static Response delete(String jsonName){
        return delete(jsonName,TimeEntryResponse.class,setParamsDelete());
    }
    private static Map<String, String> setParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("api-key",API_KEY.get());
        params.put("workspaceId","633f5b7689bf9c24493ee39d");
        params.put("userId",ID_PROJ.get());
        return params;
    }
    private static Map<String, String> setParamsPost(Map<String, String> params) {
        params.put("projectId", ID_PROJ.get());
        return params;

    }

    private static Map<String, String> setParamsPut() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("api-key",API_KEY.get());
        params.put("workspaceId","633f5b7689bf9c24493ee39d");
        params.put("id",TIME_ENTRY_ID.get());
        params.put("description",DESCRIPTION.get());
        return params;

    }

    private static Map<String, String> setParamsDelete() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("api-key",API_KEY.get());
        params.put("workspaceId","633f5b7689bf9c24493ee39d");
        params.put("id",TIME_ENTRY_ID.get());
        return params;

    }


}
